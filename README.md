<h1> DFC Feedback </h1>

<h2> How to submit a new idea or improvement via an issue: </h2>

First navigate to the issues tab by clicking on issues in the left navigation bar.


![Creating a new issue](doc/main_board.png)

Click on the green button in the top-right corner that reads "Add Issue" or "New Issue".

![Creating a new issue](doc/select_template.png)

Depending on wheter or not you want to pitch an improvement or a new idea to us select the right template,
and fill out the sections between these brackets: { }. You can remove the placeholder text that is already there.
If you like to see how your issue will look click on the preview button above the description text.

Well done, you've added an issue and the board will review it.
