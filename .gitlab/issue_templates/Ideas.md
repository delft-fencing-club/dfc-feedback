<!--- Please edit the existing template (This is a comment you don't need to delete this) --->

<h3>Your idea:</h3>

***

<h4>Description:</h4>
{Describe here what your idea is}

***

<h4>Underpinning:</h4>
{Describe why you think your idea should be implemented}

***

<h4>Execution:</h4>
{Describe how you expect this idea to be executed}

***


/milestone %"New Ideas"
/label ~"To Do"
/assign @Secretaris_DFC @TDFC @DFC-Voorzitter
/subscribe